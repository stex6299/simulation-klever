//
// ********************************************************************
// * License and Disclaimer                                           *
// *                                                                  *
// * The  Geant4 software  is  copyright of the Copyright Holders  of *
// * the Geant4 Collaboration.  It is provided  under  the terms  and *
// * conditions of the Geant4 Software License,  included in the file *
// * LICENSE and available at  http://cern.ch/geant4/license .  These *
// * include a list of copyright holders.                             *
// *                                                                  *
// * Neither the authors of this software system, nor their employing *
// * institutes,nor the agencies providing financial support for this *
// * work  make  any representation or  warranty, express or implied, *
// * regarding  this  software system or assume any liability for its *
// * use.  Please see the license in the file  LICENSE  and URL above *
// * for the full disclaimer and the limitation of liability.         *
// *                                                                  *
// * This  code  implementation is the result of  the  scientific and *
// * technical work of the GEANT4 collaboration.                      *
// * By using,  copying,  modifying or  distributing the software (or *
// * any work based  on the software)  you  agree  to acknowledge its *
// * use  in  resulting  scientific  publications,  and indicate your *
// * acceptance of all terms of the Geant4 Software license.          *
// ********************************************************************
//
//
/// \file EventAction.hh
/// \brief Definition of the B1::EventAction class

#ifndef B1EventAction_h
#define B1EventAction_h 1

#include "G4UserEventAction.hh"
#include "globals.hh"

/// Event action class
///

//namespace B1
//{

class RunAction;

class EventAction : public G4UserEventAction
{
  public:
    EventAction(RunAction* runAction);
    ~EventAction() override;

    void BeginOfEventAction(const G4Event* event) override;
    void EndOfEventAction(const G4Event* event) override;
	
	
	// PMG Sim
    void AddDepositedEnergyGenni(G4double Edep) {E_Genni+=Edep;}
    void AddDepositedEnergyJack(G4double Edep) {E_Jack+=Edep;}

    void AddDepositedEnergyLG1(G4double Edep) {E_LG1 += Edep;}
    void AddDepositedEnergyLG2(G4double Edep) {E_LG2 += Edep;}
    void AddDepositedEnergyLG3(G4double Edep) {E_LG3 += Edep;}
    void AddDepositedEnergyLG4(G4double Edep) {E_LG4 += Edep;}
    void AddDepositedEnergyLG5(G4double Edep) {E_LG5 += Edep;}
    void AddDepositedEnergyLG6(G4double Edep) {E_LG6 += Edep;}
    void AddDepositedEnergyLG7(G4double Edep) {E_LG7 += Edep;}


  private:
    RunAction* fRunAction;
	
	
	// PMG Sim
	G4double E_Genni;
	G4double E_Jack;
	
	// Note! Not in this order
	G4double E_LG1;
	G4double E_LG2;
	G4double E_LG3;
	G4double E_LG4;
	G4double E_LG5;
	G4double E_LG6;
	G4double E_LG7;
	
	
	
	
};

//}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

#endif


