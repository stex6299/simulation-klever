//
// ********************************************************************
// * License and Disclaimer                                           *
// *                                                                  *
// * The  Geant4 software  is  copyright of the Copyright Holders  of *
// * the Geant4 Collaboration.  It is provided  under  the terms  and *
// * conditions of the Geant4 Software License,  included in the file *
// * LICENSE and available at  http://cern.ch/geant4/license .  These *
// * include a list of copyright holders.                             *
// *                                                                  *
// * Neither the authors of this software system, nor their employing *
// * institutes,nor the agencies providing financial support for this *
// * work  make  any representation or  warranty, express or implied, *
// * regarding  this  software system or assume any liability for its *
// * use.  Please see the license in the file  LICENSE  and URL above *
// * for the full disclaimer and the limitation of liability.         *
// *                                                                  *
// * This  code  implementation is the result of  the  scientific and *
// * technical work of the GEANT4 collaboration.                      *
// * By using,  copying,  modifying or  distributing the software (or *
// * any work based  on the software)  you  agree  to acknowledge its *
// * use  in  resulting  scientific  publications,  and indicate your *
// * acceptance of all terms of the Geant4 Software license.          *
// ********************************************************************
//
//
/// \file DetectorConstruction.cc
/// \brief Implementation of the B1::DetectorConstruction class

#include "DetectorConstruction.hh"

#include "G4RunManager.hh"
#include "G4NistManager.hh"
#include "G4Box.hh"
#include "G4Cons.hh"
#include "G4Orb.hh"
#include "G4Sphere.hh"
#include "G4Trd.hh"
#include "G4LogicalVolume.hh"
#include "G4SystemOfUnits.hh"
#include "G4RegionStore.hh"

#include "G4LogicalVolumeStore.hh"
#include "G4UniformMagField.hh"
#include "G4FieldManager.hh"
#include "G4TransportationManager.hh"


// Color
#include "G4VisAttributes.hh"
#include "G4Colour.hh"





//namespace B1
//{

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

DetectorConstruction::DetectorConstruction()
{}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

DetectorConstruction::~DetectorConstruction()
{}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

G4VPhysicalVolume* DetectorConstruction::Construct()
{
    // Get nist material manager
    G4NistManager* nist = G4NistManager::Instance();

    G4Material* Silicon = nist->FindOrBuildMaterial("G4_Si");
    G4Material* Copper= nist->FindOrBuildMaterial("G4_Cu");    
    G4Material* Lead= nist->FindOrBuildMaterial("G4_Pb");

    G4Material* Plastic= nist->FindOrBuildMaterial("G4_PLASTIC_SC_VINYLTOLUENE");
    G4Material* BGO = nist->FindOrBuildMaterial("G4_BGO");

	
	//Unused materials
    //G4Material* Iridium = nist->FindOrBuildMaterial("G4_Ir");
    //G4Material* Tungsten = nist->FindOrBuildMaterial("G4_W");

    G4double density;
    G4int   ncomponents;
    G4int natoms;
    // This function illustrates the possible ways to define materials using
    // G4 database on G4Elements
    G4Material* PbO = new G4Material("PbO", density=  9.530 *g/cm3, ncomponents=2);
    G4Element* O  = nist->FindOrBuildElement(8);
    PbO->AddElement(O , natoms=1);
    G4Element* Pb = nist->FindOrBuildElement(82);
    PbO->AddElement(Pb, natoms= 1);

    G4bool checkOverlaps = true;

    G4Material* world_mat = nist->FindOrBuildMaterial("G4_AIR");
	
	
	
	
	
	
	
	// Ste - CsI crystal
	G4double A, Z;
	
	G4Element* elI  = new G4Element("Iodine",  "I",  Z=53., A= 126.90447*g/mole);
	G4Element* elCs = new G4Element("Cesium",  "Cs", Z=55., A= 132.90543*g/mole);

	density = 4.51 *g/cm3;
	G4Material* CsI = new G4Material("CsI", density, ncomponents= 2);
	
	CsI-> AddElement(elCs, natoms=1);
	CsI-> AddElement(elI,  natoms=1);
	
	// Ste - NaI crystal
	G4Element* elNa = new G4Element("Sodium",  "Na", Z=11., A= 22.989768*g/mole);
	
	density = 3.67 *g/cm3;
	G4Material* NaI = new G4Material("NaI", density, ncomponents= 2);
	
	NaI-> AddElement(elNa, natoms=1);
	NaI-> AddElement(elI,  natoms=1);
	
	
	// PbF2 (SPero vada bene
	G4Element* F = nist->FindOrBuildElement(9);
	
	density = 7.770 *g/cm3;
	
	G4Material* PbF2 = new G4Material("PbF2", density, ncomponents= 2);
	
	PbF2->AddElement(Pb, natoms=1);
	PbF2->AddElement(F, natoms=2);
		
		
		

    //-- WORLD
    G4Box* solidWorld = new G4Box("World",10*m,10*m,50*m);
    G4LogicalVolume* logicWorld = new G4LogicalVolume(solidWorld, world_mat, "World");
    G4VPhysicalVolume* physWorld = new G4PVPlacement
                        (0,                    // no rotation
                        G4ThreeVector(),       // centre position
                        logicWorld,            // its logical volume
                        "World",               // its name
                        0,                     // its mother volume
                        false,                 // no boolean operation
                        0,                     // copy number
                        checkOverlaps);        // overlaps checking
						
						
	// Il mondo deve essere nascosto, sennò non vedo dentro
	G4VisAttributes* worldColor= new G4VisAttributes(G4Colour(1.,1.,1.));
	worldColor->SetVisibility(false);
	logicWorld->SetVisAttributes(worldColor);
	
	
	
	
	
	
//=================================================================================================================	
// Begin of PMG-Klever simulation




//=================================================================================================================
// Begin of Straight line

//-----------------------------------------------------------------------------------------------------------------
    // Telescope 1
	G4Box* Tele1 = new G4Box("Tele1",2*cm/2, 2*cm/2, 410*um/2);
    G4LogicalVolume* fLogicTele1 = new G4LogicalVolume(Tele1, Silicon,"Tele1");
    new G4PVPlacement(0, 
					G4ThreeVector(0, 0, 0.*m), 
					fLogicTele1, 
					"Tele1", 
					logicWorld, 
					false, 
					0, 
					checkOverlaps);


	
//-----------------------------------------------------------------------------------------------------------------
    // Telescope 2
	G4Box* Tele2 = new G4Box("Tele2",2*cm/2, 2*cm/2, 410*um/2);
    G4LogicalVolume* fLogicTele2 = new G4LogicalVolume(Tele2, Silicon,"Tele2");
    new G4PVPlacement(0, 
					G4ThreeVector(0, 0, 410*um/2 + 13.65*m), 
					fLogicTele2, 
					"Tele2", 
					logicWorld, 
					false, 
					0, 
					checkOverlaps);



//-----------------------------------------------------------------------------------------------------------------
    // Bremsstrahlung target
	
	// Spessore in unità di X0
	//G4double thicknessConvRame = 0.2 * Copper->GetRadlen();

	G4Box* Rame = new G4Box("BStarget",10*cm/2, 10*cm/2, 1*mm/2);
	G4LogicalVolume* fLogicRame = new G4LogicalVolume(Rame, Copper,"BStarget");
	new G4PVPlacement(0, 
					G4ThreeVector(0., 0, 13.65*m + 40*cm + 38*cm + 1*mm/2), 
					fLogicRame, 
					"BStarget", 
					logicWorld, 
					false, 
					0, 
					checkOverlaps);



//-----------------------------------------------------------------------------------------------------------------
    // Beam Chamber 1
	G4Box* BC1 = new G4Box("BC1",2*cm/2, 2*cm/2, 410*um/2);
    G4LogicalVolume* fLogicBC1 = new G4LogicalVolume(BC1, Silicon,"BC1");
    new G4PVPlacement(0, 
					G4ThreeVector(0, 0, 13.6*m + 5.25*m), 
					fLogicBC1, 
					"BC1", 
					logicWorld, 
					false, 
					0, 
					checkOverlaps);



//-----------------------------------------------------------------------------------------------------------------
    // Magnet
    G4Box* Magnet = new G4Box("Magnet",40*cm/2, 40*cm/2, 2*m/2); // TODO: Check other dimension!!
    G4LogicalVolume* logicMagnet = new G4LogicalVolume(Magnet,world_mat,"Magnet");
    new G4PVPlacement(0,
					G4ThreeVector(0, 0, 13.6*m + 5.25*m + 65*cm + 2*m/2),
					logicMagnet,
					"Magnet",
					logicWorld,
					false,
					0,
					checkOverlaps);




//-----------------------------------------------------------------------------------------------------------------
    // PbF2 Crystal
	//Spessore in unità di X0
	G4double thicknessCrist = 2 * PbF2->GetRadlen(); 

	
    G4Box* Crystal = new G4Box("Crystal", 1*PbF2->GetRadlen()/2, 2*PbF2->GetRadlen()/2, thicknessCrist/2); 
    G4LogicalVolume* logicCrystal = new G4LogicalVolume(Crystal,PbF2,"Crystal");
    new G4PVPlacement(0,
					G4ThreeVector(0, 0, 13.6*m + 5.25*m + 13.71*m - 7*cm - 5*cm - 3*cm - 3.79*m - thicknessCrist / 2),
					logicCrystal,
					"Crystal",
					logicWorld,
					false,
					0,
					checkOverlaps);



//-----------------------------------------------------------------------------------------------------------------
    // Scinti Canadese
    G4Box* ScintiCanada = new G4Box("ScintiCanada",10*cm/2, 10*cm/2, 1*cm/2);// to check!!!
    G4LogicalVolume* fLogicScintiCanada = new G4LogicalVolume(ScintiCanada, Plastic,"ScintiCanada");
    new G4PVPlacement(0, 
					G4ThreeVector(0*cm, 0, 8.87*m + 95*cm - 11.5*cm), 
					fLogicScintiCanada, 
					"ScintiCanada", 
					logicWorld, 
					false, 
					0, 
					checkOverlaps);



//-----------------------------------------------------------------------------------------------------------------
    // Beam Chamber 2
	G4Box* BC2 = new G4Box("BC2",2*cm/2, 2*cm/2, 410*um/2);
    G4LogicalVolume* fLogicBC2 = new G4LogicalVolume(BC2, Silicon,"BC2");
    new G4PVPlacement(0, 
					G4ThreeVector(0, 0, 13.6*m + 5.25*m + 13.71*m), 
					fLogicBC2, 
					"BC2", 
					logicWorld, 
					false, 
					0, 
					checkOverlaps);




//=================================================================================================================
// Begin of Calorimeter
	
	G4double distCalo = 13.65*m + 5.25*m + 13.71*m + 37*cm;

	// Centro di genni
	G4double x0_genni = 0*cm;
	G4double z0_genni = distCalo + 32*cm/2;



//-----------------------------------------------------------------------------------------------------------------
    // Genni (Non ho capito come sia messo)
	//					G4ThreeVector(0, 0, distCalo + 32*cm/2), 
    G4Box* Genni = new G4Box("Genni",15*cm/2, 15*cm/2, 32*cm/2);
    G4LogicalVolume* fLogicGenni = new G4LogicalVolume(Genni, BGO,"Genni");
    new G4PVPlacement(0, 
					G4ThreeVector(x0_genni, 0, z0_genni), 
					fLogicGenni, 
					"Genni", 
					logicWorld, 
					false, 
					0, 
					checkOverlaps); 



//-----------------------------------------------------------------------------------------------------------------
    // Jack (Photons) //TODO: Don't know how to do
	// Per ora è l'ottavo lead glass, non è uno shashlik
    // G4Box* Jack = new G4Box("Jack", 10*cm/2, 10*cm/2, 39*cm/2);
    // G4LogicalVolume* fLogicJack = new G4LogicalVolume(Jack, PbO,"Jack");
    // new G4PVPlacement(0, 
					// G4ThreeVector(x0_genni + 17.3*cm, .25*cm, z0_genni + 31*cm), 
					// fLogicJack, 
					// "Jack", 
					// logicWorld, 
					// false, 
					// 0, 
					// checkOverlaps); 
                    
                    
                    
    // Real Jack
    G4double offsetJack = 0*cm;  // TODO: Bisogna sommare qualcosa per allinearlo in z
    
    G4Box* JackPlastic = new G4Box("JackPlastic", 10*cm/2, 10*cm/2, 4*mm/2);
    G4LogicalVolume* fLogicJackPlastic = new G4LogicalVolume(JackPlastic, Plastic,"JackPlastic");
    
    G4Box* JackLead = new G4Box("JackLead", 10*cm/2, 10*cm/2, 1.5*mm/2);
    G4LogicalVolume* fLogicJackLead = new G4LogicalVolume(JackLead, Lead,"JackLead"); //TODO: PUT RIGHT MATERIAL
    
    // Devo mettere 70 scintillatori e 69 piombi
    for (int i=0 ; i<70 ; i++) {
        
        new G4PVPlacement(0, 
            G4ThreeVector(x0_genni + 17.3*cm, .25*cm, z0_genni + offsetJack + i * 5.5*mm), 
            fLogicJackPlastic, 
            "JackPlastic",
            logicWorld,
            false,
            i, // Non sono sicuro sia vitale. Anzi, non serve
            checkOverlaps);
        
        new G4PVPlacement(0, 
            G4ThreeVector(x0_genni + 17.3*cm, .25*cm, z0_genni + offsetJack + (5.5/2)*mm + i * 5.5*mm), // Sommo metà del blocco
            fLogicJackLead, 
            "JackLead",
            logicWorld,
            false,
            i, // Non sono sicuro sia vitale. Anzi, non serve
            checkOverlaps);
        
        
        
    }
    
    
    
    // Ultimo scinti
     new G4PVPlacement(0, 
        G4ThreeVector(x0_genni + 17.3*cm, .25*cm, z0_genni + offsetJack + 70 * 5.5*mm), 
        fLogicJackPlastic, 
        "JackPlastic",
        logicWorld,
        false,
        75, // Non sono sicuro sia vitale. Anzi, non serve
        checkOverlaps);

    
    
    




//-----------------------------------------------------------------------------------------------------------------
	// Matrice di rotazione
	//NOTA PER IL ME DEL FUTURO: rad = 1,https://geant4.web.cern.ch/sites/default/files/geant4/collaboration/working_groups/electromagnetic/gallery/units/SystemOfUnits.h.html
	
				// // G4double Angle = .5*rad; // TODO: Inserire vero angolo in radianti 

				// // G4RotationMatrix *xRot = new G4RotationMatrix;
				// // xRot->rotateY(-Angle);
				
				
				// // // Dal 7 in poi
				// // G4double x0_LG = 15/2*cm + 15*cm + 2*10*cm + 11/2*cm + 20*cm; // Il 20 è a caso
				// // G4double z0_LG = distCalo + 20*cm + 40/2*cm;
	
	
//-----------------------------------------------------------------------------------------------------------------
	// Matrici di rotazione
	
	// Secondo lead glass
	G4double angle1 = 2.9*degree ; // LG1 (secondo)
	G4RotationMatrix *xRot1 = new G4RotationMatrix;
	xRot1->rotateY(-angle1);
	
	
	// Ultimi 5
	G4double angle2 = 4.5*degree ; // Gli ultimi 5
	G4RotationMatrix *xRot2 = new G4RotationMatrix;
	xRot2->rotateY(-angle2);
	
	
	

	
	
	

//-----------------------------------------------------------------------------------------------------------------
    // LG 2
    G4Box* LG2 = new G4Box("LG2", 10*cm/2, 10*cm/2, 40*cm/2);
    G4LogicalVolume* fLogicLG2 = new G4LogicalVolume(LG2, PbO,"LG2");
    new G4PVPlacement(0, 
					G4ThreeVector(x0_genni + 32.9*cm, -1*cm, z0_genni + 32.2*cm), 
					fLogicLG2, 
					"LG2", 
					logicWorld, 
					false, 
					0, 
					checkOverlaps); 



//-----------------------------------------------------------------------------------------------------------------
    // LG 1			
    G4Box* LG1 = new G4Box("LG1", 10*cm/2, 10*cm/2, 40*cm/2);
    G4LogicalVolume* fLogicLG1 = new G4LogicalVolume(LG1, PbO,"LG1");
    new G4PVPlacement(xRot1, 
					G4ThreeVector(x0_genni + 43.5*cm, -1*cm, z0_genni + 31.7*cm), 
					fLogicLG1, 
					"LG1", 
					logicWorld, 
					false, 
					0, 
					checkOverlaps); 


//-----------------------------------------------------------------------------------------------------------------
    // LG 7
	
	//Centro del terzo lead glass
	// TODO: Insert real values
	G4double x0_LG = x0_genni + 56.6*cm;
	G4double z0_LG = z0_genni + 30.5*cm;
	
    G4Box* LG7 = new G4Box("LG7", 11*cm/2, 11*cm/2, 40*cm/2);
    G4LogicalVolume* fLogicLG7 = new G4LogicalVolume(LG7, PbO,"LG7");
    new G4PVPlacement(xRot2, 
					G4ThreeVector(x0_LG, -1.5*cm, z0_LG), 
					fLogicLG7, 
					"LG7", 
					logicWorld, 
					false, 
					0, 
					checkOverlaps); 


//-----------------------------------------------------------------------------------------------------------------
    // LG 5
    G4Box* LG5 = new G4Box("LG5", 11*cm/2, 11*cm/2, 40*cm/2);
    G4LogicalVolume* fLogicLG5 = new G4LogicalVolume(LG5, PbO,"LG5");
    new G4PVPlacement(xRot2, 
					G4ThreeVector(x0_LG + 11*cm*cos(angle2), -1.5*cm, z0_LG - 11*cm*sin(angle2)), 
					fLogicLG5, 
					"LG5", 
					logicWorld, 
					false, 
					0, 
					checkOverlaps); 


//-----------------------------------------------------------------------------------------------------------------
    // LG 6
    G4Box* LG6 = new G4Box("LG6", 11*cm/2, 11*cm/2, 40*cm/2);
    G4LogicalVolume* fLogicLG6 = new G4LogicalVolume(LG6, PbO,"LG6");
    new G4PVPlacement(xRot2, 
					G4ThreeVector(x0_LG + 2*11*cm*cos(angle2), -1.5*cm, z0_LG - 2*11*cm*sin(angle2)), 
					fLogicLG6, 
					"LG6", 
					logicWorld, 
					false, 
					0, 
					checkOverlaps); 


//-----------------------------------------------------------------------------------------------------------------
    // LG 3
    G4Box* LG3 = new G4Box("LG3", 11*cm/2, 11*cm/2, 40*cm/2);
    G4LogicalVolume* fLogicLG3 = new G4LogicalVolume(LG3, PbO,"LG3");
    new G4PVPlacement(xRot2, 
					G4ThreeVector(x0_LG + 3*11*cm*cos(angle2), -1.5*cm, z0_LG - 3*11*cm*sin(angle2)), 
					fLogicLG3, 
					"LG3", 
					logicWorld, 
					false, 
					0, 
					checkOverlaps); 


//-----------------------------------------------------------------------------------------------------------------
    // LG 4 
    G4Box* LG4 = new G4Box("LG4", 11*cm/2, 11*cm/2, 40*cm/2);
    G4LogicalVolume* fLogicLG4 = new G4LogicalVolume(LG4, PbO,"LG4");
    new G4PVPlacement(xRot2, 
					G4ThreeVector(x0_LG + 4*11*cm*cos(angle2), -1.5*cm, z0_LG - 4*11*cm*sin(angle2)), 
					fLogicLG4, 
					"LG4", 
					logicWorld, 
					false, 
					0, 
					checkOverlaps); 


	
// END OF SETUP
//=================================================================================================================
//					G4ThreeVector(x0_LG - 4*11*cm*cos(CLHEP::pi/2*rad - Angle), 0, z0_LG + 4*11*cm*sin(CLHEP::pi/2*rad - Angle)), 

	




	
	
	
	
	
	
	
	
	// Colori dei vari pezzi
	// TODO: Aggiungere questo setup, sono quelli vecchi

	
	// Scinti = blu
	G4VisAttributes* scintiColor = new G4VisAttributes(G4Colour(0.,0.,1., 1.));
	scintiColor->SetVisibility(true);
	scintiColor->SetForceSolid(true);
	
	//fLogicScintillator1->SetVisAttributes(scintiColor);
	//fLogicScintillator2->SetVisAttributes(scintiColor);


	// Silici = Verdi
	G4VisAttributes* siliColor = new G4VisAttributes(G4Colour(0.,1.,0., 1.));
	siliColor->SetVisibility(true);
	siliColor->SetForceSolid(true);
	
	fLogicTele1->SetVisAttributes(siliColor);
	fLogicTele2->SetVisAttributes(siliColor);
	fLogicBC1->SetVisAttributes(siliColor);
	fLogicBC2->SetVisAttributes(siliColor);
	

	
	// Rame+cristallo+canada = Rosso
	G4VisAttributes* absColor = new G4VisAttributes(G4Colour(1.,0.,0., 1.));
	absColor->SetVisibility(true);
	absColor->SetForceSolid(true);

	fLogicRame->SetVisAttributes(absColor);
	logicCrystal->SetVisAttributes(absColor);
	fLogicScintiCanada->SetVisAttributes(absColor);
	
	
	// Magnet = yellow
	G4VisAttributes* magColor = new G4VisAttributes(G4Colour(1.,1.,0., .5));
	magColor->SetVisibility(true);
	magColor->SetForceSolid(true);
	
	logicMagnet->SetVisAttributes(magColor);
    
    
    // Plastica Jack = Gialla
    fLogicJackPlastic->SetVisAttributes(magColor);
    // Piombo Jack = Rosso
    fLogicJackLead->SetVisAttributes(absColor);


		
		
		
		
		
	// Calo A = Verdi
	G4VisAttributes* caloA = new G4VisAttributes(G4Colour(0.,1.,0., 1.));
	caloA->SetVisibility(true);
	caloA->SetForceSolid(true);
	
	fLogicGenni->SetVisAttributes(caloA);
	fLogicLG2->SetVisAttributes(caloA);
	fLogicLG7->SetVisAttributes(caloA);
	fLogicLG6->SetVisAttributes(caloA);
	fLogicLG4->SetVisAttributes(caloA);
	
	// Calo B = Blu
	G4VisAttributes* caloB = new G4VisAttributes(G4Colour(0.,0.,1., 1.));
	caloB->SetVisibility(true);
	caloB->SetForceSolid(true);
	
	//fLogicJack->SetVisAttributes(caloB);
	fLogicLG1->SetVisAttributes(caloB);
	fLogicLG5->SetVisAttributes(caloB);
	fLogicLG3->SetVisAttributes(caloB);
	


	


	
	

  //always return the physical World
  return physWorld;
}

void DetectorConstruction::ConstructSDandField()
{
    // =============================
    //       MAGNETIC FIELD
    // =============================
    G4double fieldValue = 4.05778*tesla*m / (2*m);
    G4UniformMagField* myField = new G4UniformMagField(G4ThreeVector(0., fieldValue, 0.));
    G4FieldManager* fieldMgr = G4TransportationManager::GetTransportationManager()->GetFieldManager();
    G4LogicalVolume* logicBox1 = G4LogicalVolumeStore::GetInstance()->GetVolume("Magnet");
    G4FieldManager* localfieldMgr = new G4FieldManager(myField);
    logicBox1->SetFieldManager(localfieldMgr,true);
    fieldMgr->CreateChordFinder(myField);
}
//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

//}
